//! Variable Length Message Library
//!
//! This library aids in making variable length messages as dynamically sized
//! types (DSTs).

// Do not require the standard library. Useful for constrained environments 
// like UEFI.
#![no_std]
#![warn(missing_docs)]

// Import the alloc create to use the Rust memory allocation APIs
extern crate alloc;

use {
    alloc::alloc::alloc,
    core::{
        ptr,
        slice,
        mem::size_of,
        alloc::Layout,
    }
};

/// A message with a generic header and embdded variable length payload.
#[repr(C)]
#[derive(Debug)]
pub struct Message<Header> {

    /// The message header
    pub header: Header,

    /// The message payload, serialized as an embedded byte array
    pub payload: [u8],

}

impl<Header> Message<Header> {

    /// Create a new message from a given header and payload.
    pub fn new<Payload>(h: Header, p: Payload) ->  &'static mut Message<Header> {

        let v: &mut Message<Header> = unsafe {

            // allocate space
            let len = size_of::<Header>() + size_of::<Payload>();
            let x = ptr::NonNull::new_unchecked(
                alloc(Layout::from_size_align_unchecked(len, 0))
            );

            // write data
            ptr::write_unaligned(x.as_ptr().cast(), h);
            ptr::write_unaligned(x.as_ptr().add(size_of::<Header>()).cast(), p);

            // XXX needs to be a fat pointer
            //x.as_ptr() as *mut Message<Header>

            // https://users.rust-lang.org/t/construct-fat-pointer-to-struct/29198/9
            // note: the fat pointer size is just for the trailing DST bit ([u8])
            let fat_sz = size_of::<Payload>(); 
            let s = slice::from_raw_parts_mut(x.as_ptr() as *mut u8, fat_sz);
            let p = s as *mut [u8] as *mut Self;

            // ptr to ref ("safer" transmute alternative)
            // https://doc.rust-lang.org/std/mem/fn.transmute.html
            let r =  &mut *(p as *mut Self as *mut Self);
            r

        };
        v

    }

}

#[cfg(test)]
#[macro_use]
extern crate std;

//rustc seems to get these wrong for test code
#[allow(dead_code, unused_imports)] 
mod tests {

    use super::*;
    use core::convert::TryInto;

    #[test]
    fn construct_message() {

        #[derive(Debug)]
        #[repr(C)]
        struct TestHeader { a: u64, b: u8, c: u16 }

        //Does not work without packed! but pretty sure we want this for packets 
        #[repr(packed)] 
        struct TestPayload { p: u32, q: u8, r: u16, s: u64 }

        println!("{}, {}", size_of::<TestHeader>(), size_of::<TestPayload>());

        let msg = Message::new(
            TestHeader{ a: 1, b: 2, c: 3 },
            TestPayload{ p: 6, q: 7, r: 8, s: 9 },
        );

        println!("{:?}", msg);

        assert_eq!(msg.header.a, 1);
        assert_eq!(msg.header.b, 2);
        assert_eq!(msg.header.c, 3);

        assert_eq!(
            u32::from_le_bytes(msg.payload[0..4].try_into().unwrap()), 6);
        assert_eq!(msg.payload[4], 7);
        assert_eq!(
            u16::from_le_bytes(msg.payload[5..7].try_into().unwrap()), 8);
        assert_eq!(
            u64::from_le_bytes(msg.payload[7..15].try_into().unwrap()), 9);

        println!("{:p}", &msg.header.c);
        println!("{:p}", &msg.payload[0]);

    }

}
