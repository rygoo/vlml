Variable Length Message Library
===============================

This is just a little experiment for making variable length messages as
[dynamically sized types](https://doc.rust-lang.org/nomicon/exotic-sizes.html)
(DSTs). The primary use is for implementing wire protocols.
